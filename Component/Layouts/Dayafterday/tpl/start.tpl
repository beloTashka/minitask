<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="keywords" content="{$keywords}">
        <meta name="description" content="{$description}">
        {$canonical}
        {$prev}
        {$next}
        <link rel="stylesheet" href="/Component/Layouts/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="/Component/Layouts/css/style.css">
		<link rel="stylesheet" href="/Component/fonts/font-awesome-4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="/Component/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <script src="/Component/Layouts/js/jquery-1.6.4.min.js"></script>  
        <script src="/Component/Layouts/js/scroll-startstop.events.jquery.js"></script>  
        <script src="/Component/Layouts/Dayafterday/js/start.js"></script>  
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <!-- Include custom js and css -->
        {$js}
        {$css}
        <style type="text/css">
            :root {
                --main-bg-color: #f7f8fa; 
                --top-attn-color: #acbcd3;
                --top-brigth-color: #f7f8fa;  
                --main-border-color: #5d80b6;
                --main-attn-color: #101f5a;
                --main-std-color: #101f5a;
                --main-st-color: #5d80b6;
                --form-error-color: #b43a47;
                --form-message-color: #5c7943;
            }
        </style>
    
        <title>{$title}</title>
    </head>
    <body class="catalog" itemscope="" itemtype="http://schema.org/{$type_page}">
        {include top_off_it_1.tpl}
        {include top_off_it_2_llink.tpl}
        {include top_on_it.tpl}

        <!--<div class="container fixed-top p-2 mb-2 bg-secondary text-white invisible" id="top">The menu is always at hand...</div>-->
        <div class="main-container block-row flex-row">
            {$content}
        </div><!--/container-->


    </body>
</html>
