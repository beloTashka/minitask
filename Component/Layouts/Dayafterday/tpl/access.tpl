<h1>Раздел не доступен для свободного просмотра</h1>
<div class="row flex-row width100 text-center content-text"> 
    <div class="width100"> 
        <div class="width100"> 
            <div class="text-center brim_bottom_20"> 
                 <div>Введите Ваш логин и пароль</div> 
            </div>
            <div class="text-center brim_bottom_20 error"> 
                 <div>{$error_message}</div> 
            </div> 
            <div class="text-center brim_bottom_20 green"> 
                 <div>{$green_message}</div> 
            </div>             
            <div class="width100 form-message"> 
                <form action="" role="form" method="post" id="form-feedback"> 
                    <div class="row text-center"> 
                        <div class="col-md-4 brim_0_15"> 
                            <div class="width100"> 
                                <div class="form-group width100"> 
                                    <label for="input{$name}" class="sr-only">{$title}</label> 
                                    <input type="text" class="form-control" id="inputLogin" name="login" value="{$value}" placeholder="ваше {$title}" maxlength="{$limitTo}"> 
                                </div>
                                <div class="form-group width100"> 
                                    <label for="input{$name}" class="sr-only">{$title}</label> 
                                    <input type="password" class="form-control {$class_error}" id="inputPass" name="pass" value="{$value}" placeholder="ваше {$title}" maxlength="{$limitTo}"> 
                                </div> 
                            </div>
                        </div> 
                    </div> 
                    <div class="row flex-combined-row"> 
                        <div class="gray-line col-xs-2 col-md-4"> 
                            <div id="feedback_answer" class="width100"></div> 
                        </div>
                        <div class="form-group text-center col-xs-8 col-md-4 form-button"> 
                            <div class="width100"> 
                                <div class="row button"> 
                                    <div class="col-sx width100"> 
                                        <button type="submit" id="place_order" class="width100">Отправить</button> 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="gray-line col-sx col-md-4"> </div> 
                    </div> 
                </form> 
            </div> 
        </div> 
    </div> 
</div>


