<h1>Обсудить сотрудничество</h1>
<div class="row flex-row width100 text-center content-text"> 
    <div class="width100"> 
        <div class="width100"> 
            <div class="text-center brim_bottom_20"> 
                 
                <div>Укажите ваши контактные данные и я отвечу ближайшее время</div> 
            </div> 
            <div class="width100 form-message"> 
                <form action="" role="form" method="post" id="form-feedback"> 
                    <div class="row"> 
                        <div class="col-md-4 brim_0_15"> 
                            <div class="width100"> 
                                <div class="form-group width100"> 
                                    <label for="inputName" class="sr-only">Имя</label> 
                                    <input type="text" class="form-control" id="inputName" name="name" value="" placeholder="ваше имя" maxlength="70"> 
                                </div> 
                                <div class="form-group width100"> 
                                    <label for="inputEmail" class="sr-only">Email</label> 
                                    <input type="email" class="form-control" id="inputEmail" name="email" value="" placeholder="ваш Email" maxlength="70"> 
                                </div> 
                                <div class="form-group width100"> 
                                    <label for="inputPhone" class="sr-only">Телефон</label> 
                                    <input type="tel" class="form-control" id="inputPhone" name="phone" value="" placeholder="ваш телефон" maxlength="18"> 
                                </div> 
                            </div>
                        </div> 
                        <div class="col-md-8"> 
                            <div class="width100"> 
                                <div class="form-group width100"> 
                                    <label for="inputNote" class="sr-only">Текст вашего сообщения</label> 
                                    <textarea rows="5" class="form-control" id="inputNote" name="note" placeholder="ваше сообщение" maxlength="1000"></textarea> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="row flex-combined-row"> 
                        <div class="gray-line col-xs-2 col-md-4"> 
                            <div id="feedback_answer" class="width100"></div> 
                        </div>
                        <div class="form-group text-center col-xs-8 col-md-4 form-button"> 
                            <div class="width100"> 
                                <div class="row button"> 
                                    <div class="col-sx width100"> 
                                        <button type="submit" id="place_order" class="width100">Отправить</button> 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <div class="gray-line col-sx col-md-4"> </div> 
                    </div> 
                </form> 
            </div> 
        </div> 
    </div> 
</div>


