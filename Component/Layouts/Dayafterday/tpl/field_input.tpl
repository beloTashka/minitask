<div class="form-group width100"> 
    <label for="input{$name}" class="sr-only">{$title}</label> 
    <input type="{$type}" class="form-control {$class_error}" id="input{$name}" name="{$name}" value="{$value}" placeholder="ваше {$title}" maxlength="{$limitTo}"> 
</div> 