<div class="top">
    <div class="block-row flex-row">
        <div class="top-soc">
            <ul class="header-top__nav indent-left mg-tp-6 font-20">
                <li>
                    <a href="https://facebook.com/beloTashka" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="https://leetcode.com/beloTashka" target="_blank">
                        <i class="fa fa-code"></i>
                    </a>
                </li>
                <li>
                    <a href="https://gitlab.com/beloTashka" target="_blank">
                        <i class="fa fa-gitlab"></i>
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="top-contact">
            <ul class="header-top__nav indent-left">
                <li> 
                    <a href="mailto:wellwebline@gmail.com"> 
                        <i class="fa fa-envelope" aria-hidden="true"></i>wellwebline@gmail.com 
                    </a>
                </li>
                <li> 
                    <a href="skype:whitenata"> 
                        <i class="fa fa-skype" aria-hidden="true"></i>whitenata 
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>