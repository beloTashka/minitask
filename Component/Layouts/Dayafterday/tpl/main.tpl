<div class="top-container block-row flex-row">
    <h1>Эффективная разработка и сопровождение сайтов</h1> 
</div>

<div class="content-text">                                     
<div class="wrapper container-fluid block-row flex-row">
    <div class="row"> 
        <section id="project-new" class="block"> 
            <div class="project-new container"> 
                <div class="row"> 
                    <div class="col-xs-12"> 
                        <div class="heading_title text-center"> 
                            <h2 class="h2">Почему я этим занимаюсь?</h2> 
                        </div> 
                        <!-- pr blocks --> 
                        <div> 
                            <div class="row flex-combined-row"> 
                                <!-- pr block 1 --> 
                                <div class="col-xs-12 col-md-4"> 
                                    <div class="text-center">  
                                        <div>1</div> 
                                        <div> 
                                            <p>Мне это <b>нравится!</b></p>
                                            <p>Ваши сайты и Ваш успех <b>вдохновляют</b> меня.</p> 
                                            <p>Благодаря Вам, моя работа это не просто код, но и вклад в Ваш завтрашний день, который <b>Я уверена</b> будет лучше чем все предыдущие. </p> 
                                        </div> 
                                    </div> 
                                </div> 
                                <!-- pr block 1 --> 
                                <!-- pr block 2 --> 
                                <div class="col-xs-12 col-md-4"> 
                                    <div class="text-center"> 
                                        <div>2</div> 
                                        <div> 
                                            <p>У меня достаточно опыта и навыков, чтобы делать это хорошо.</p> 
                                            <p>Защитила диплом <b><a href="http://www.sevntu.ru/statyi.php?s=425" class="color-st-text">Севастопольского Национального Технического Университета</a></b></p> 
                                            <p>Чтобы лучше понимать Ваши задачи, Изучала бухгалтерию, Существующее прикладное ПО и другие области знаний, которые казались мне интересными и полезными.</p> 
                                        </div> 
                                    </div> 
                                </div> 
                                <!-- pr block 2 --> 
                                <!-- pr block 3 --> 
                                <div class="col-xs-12 col-md-4"> 
                                    <div class="text-center"> 
                                        <div>3</div> 
                                        <div> 
                                            <p>Я верю, что Ваши проекты меняют мир.</p> 
                                            <p>Он становится динамичнее, открываются новые возможности.</p> 
                                            <p>Товары становятся доступнее, Общение между людьми более живым и независящим от расстояния.</p>
                                            <p>Я хочу, чтобы  <b>изменять мир к лучшему</b> было просто. :)</p>
                                        </div> 
                                    </div> 
                                </div> 
                                <!-- pr block 3 --> 
                            </div> 
                        </div> 
                        <!-- pr blocks --> 
                    </div> 
                </div> 
            </div> 
        </section> 
    </div>
</div>
</div>

<div class="content-text color-bg-1">
    <div class="wrapper container-fluid block-row flex-row">
        <div class="container width100"> 
            <div class="heading_title text-center width100"> 
                <h2 class="h2">Ключевые навыки и технологии:</h2> 
            </div>  
            <div class="row flex-combined-row justify-content-md-center"> 
                <div class="col-md-auto"> 
                    <span class="bx-skill">PHP</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">MySql</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">JavaScript</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">jQuery</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">Ajax</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">HTML</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">CSS</span> 
                </div>
            </div>
            <div class="row flex-combined-row justify-content-md-center mg-tp-6"> 
                <div class="col-md-auto"> 
                    <span class="bx-skill">Smarty</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">Git</span> 
                </div>
            </div>
            <div class="row flex-combined-row justify-content-md-center mg-tp-6"> 
                <div class="col-md-auto"> 
                    <span class="bx-skill">MVC</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">ООП</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">SOLID</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">DRY</span> 
                </div>
                <div class="col-md-auto"> 
                    <span class="bx-skill">KISS</span> 
                </div>                
            </div> 
        </div>
    </div>
</div><!--/content-text--> 
