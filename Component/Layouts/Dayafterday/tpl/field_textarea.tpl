<div class="form-group width100"> 
    <label for="input{$name}" class="sr-only">{$title}</label> 
    <textarea class="form-control {$class_error}" id="input{$name}" name="{$name}" placeholder="ваше {$title}" maxlength="{$limitTo}">{$value}</textarea> 
</div>