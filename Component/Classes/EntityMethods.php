<?php
namespace Component;  

/**
 * Интерфейс сущности
 * 
 * Устанавливает необходимые методы для работы с текущими данными
 *
 * @name Component\EntityMethods 
 */
interface EntityMethods
{
    /**
     * Добавление новых данных
     *
     * @param string $name имя данных
     * @param string $value значение
     * @return int
     */
     public function __set($name, $value);
     
     /**
     * Получение данных
     *
     * @param string $name имя данных   
     * @return mix 
     */
     public function __get($name);
     
     /**
     * Проверка существования данных с имененм $name
     *
     * @param string $name имя данных 
     * @return bool
     */
     public function __exists($name);
  
}  