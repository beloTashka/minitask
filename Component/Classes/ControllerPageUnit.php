<?php
namespace Component;  

/**
 * Абстрактный класс реализации контроллера отображения страницы сайта
 * 
 * Устанавливает базовые свойства и методы 
 *
 * @name Component\ControllerPageUnit 
 */
abstract class ControllerPageUnit implements ControllerMethods
{      
    protected $_view;  // класс для отображения
    protected $_dataOffice; // класс управления данными
    
    use TraitDisplay; 
    use TraitUserQuery; 
    

    
    /**
    * Контент в виде html
    * @return html
    */
    public function getContent()
    {
        $tpl = $this->_dataOffice->getTplName();
        $values = $this->_dataOffice->getDataView();
        $tplContent = $this->_dataOffice->getTplContent();

        $values['content'] = $this->_view->makeHTML($tplContent, $values); 
 
        $result = $this->_view->makeHTML($tpl, $values);
    
        return $result;
    }
    
  
}  