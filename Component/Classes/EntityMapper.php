<?php
namespace Component;  

/**
 * Абстрактный класс преобразования сущности 
 * к ее структуре в реляционной db
 * 
 * Устанавливает базовые методы 
 * для работы с таблицами данных
 *
 * @name Component\EntityMapper 
 */
abstract class EntityMapper
{
    protected $_stream;
    protected $_query;
    
    protected function getStream($stream = 'sketch') 
    {
        if($stream == 'stretch') {
            $this->_stream = MySqlStretch::getInstance(); 
        } else {
            $this->_stream = MySqlSketch::getInstance();
        }
    }
    
    /**
    * Вставка строки в таблицу db
    * 
    * @param string $table
    * @param array $fields as array($field => $value, ...)
    * @param string | false $fieldId  key of table
    * 
    * @return mixed as bool false when have error and key of table if have $fieldId as sting Key
    */
    protected function insert($table, $fields, $fieldId = false) 
    {
        $result = false;
        if(is_array($fields) && $table != '') {
            
            $strFields = ''; 
            $strValues = '';
            $where = '';
            foreach($fields as $field => $value) {
                if($strFields != '') {
                    $strFields .= ', ';
                }  
                if($strValues != '') {
                    $strValues .= ', ';
                } 
                $strFields .= "`{$field}`"; 
                if(!is_null($value)) { 
                    $strValues .= "'{$value}'";
                    if($where != '') {
                        $where .= ' AND ';
                    }
                    $where .= "`{$field}` = '{$value}'";
                } else {
                    $strValues .= "NULL";
                }  
            }
            
            if($strFields != '' && $strValues != '') {
                $this->_query = "INSERT INTO `{$table}` ({$strFields}) VALUES({$strValues})";
                $this->_stream->query($this->_query);
            }
            
            if($where != '') {
                if($fieldId && $fieldId != '') {
                    $this->_query = "SELECT `{$fieldId}` FROM `{$table}` WHERE {$where} LIMIT 1"; 
                    $rowResult = $this->_stream->super_query($this->_query);   
                    if(isset($rowResult[$fieldId]) && !is_null($rowResult[$fieldId]) 
                        && $rowResult[$fieldId] != '' && $rowResult[$fieldId] != 0) {
                        $result = $rowResult[$fieldId]; 
                    }
                } else {
                    $this->_query = "SELECT * FROM `{$table}` WHERE {$where} LIMIT 1"; 
                    $rowResult = $this->_stream->super_query($this->_query);
                    if(is_array($rowResult) && count($rowResult) > 0) {
                        $result = true;
                    }
                }
            } 
        }
        return $result;
    }
    
    /**
    * Удаление строки из таблицы db
    * 
    * @param string $table
    * @param array $fields as array($field => $value, ...)
    * @param int $limit - сколько записей допускается удалить
    * 
    * @return  bool true - в таблице не осталось записей соответствующих условию,
    * false - ошибка в параметрах или наличие подобных записей в таблице 
    */
    protected function delete($table, $fields, $limit = 0)
    {
        $result = false;
        $limit = (int)$limit;
        $limit = ($limit > 0)? "LIMIT {$limit}":'';
        if(is_array($fields) && $table != '') {
            
            $where = '';
            foreach($fields as $field => $value) {
                if(!is_null($value)) { 
                    if($where != '') {
                        $where .= ' AND ';
                    }
                    $where .= "`{$field}` = '{$value}'";
                } 
            }
            $this->_query = "DELETE FROM `{$table}` WHERE {$where} {$limit}"; 
            $this->_stream->query($this->_query); 
            
            $this->_query = "SELECT * FROM `{$table}` WHERE {$where} LIMIT 1"; 
            $rowResult = $this->_stream->query_fetch($this->_query);
            if(!isset($rowResult) || !$rowResult  
                || !is_array($rowResult) || count($rowResult) == 0) {
                $result = true;
            }
        }
        return $result;
    }
    
    /**
    * Редактирование строки из таблицы db
    * 
    * @param string $table
    * @param array $fields as array($field => $value, ...)
    * @param array $where as array($field => $value, ...)
    * @param array $order as array($field => 0 | 1, ...)   0 - ASC, 1 - DESC
    * @param int $limit - сколько записей допускается удалить
    * 
    * @return  bool
    */
    protected function update($table, $fields, $where, $order = false, $limit = 0)
    {
        $result = false;
        if(is_array($fields) && $table != '' && is_array($where)) {
            
            $set = ''; 
            $strWhere = '';
            $strWhereForSelect = '';
            $strOrder = '';
            $limit = $this->buildLimit($limit);
            
            foreach($fields as $field => $value) {
                if($set != '') {
                    $set .= ', ';
                } 
                if($strWhereForSelect != '' && !is_null($value)) {
                    $strWhereForSelect .= ' AND ';
                }  
                if(!is_null($value)) { 
                    $set .= "`{$field}` = '{$value}'";
                    $strWhereForSelect .= "`{$field}` = '{$value}'";
                } else {
                    $set .= "`{$field}` = NULL";
                }  
            }
            
            foreach($where as $field => $value) {
                if(!is_null($value)) {
                    if($strWhere != '') {
                        $strWhere .= ' AND ';
                    } 
                    if($strWhereForSelect != '') {
                        $strWhereForSelect .= ' AND ';
                    } 
                    $strWhereForSelect .= "`{$field}` = '{$value}'";
                    $strWhere .= "`{$field}` = '{$value}'";
                }
            }
            
            $strOrder = $this->buildOrder($order);
             
            
            if($set != '' && $strWhere != '') {
                $this->_query = "UPDATE `{$table}` SET {$set} WHERE {$strWhere} {$strOrder} {$limit}";
                $result = $this->_stream->query($this->_query);
                
                $this->_query = "SELECT * FROM `{$table}` WHERE {$strWhereForSelect} {$strOrder} {$limit}"; 
                $arr = $this->_stream->query_fetch($this->_query); 
                if(is_array($arr) && count($arr) > 0) {
                    $result = true;
                }
            }
        }
        return $result;
    }
    
    protected function buildOrder($order = false)
    {
        $strOrder = '';
        if($order && is_array($order) && count($order) > 0) {
            foreach($order as $field => $value) {
                if($strOrder != '') {
                    $strOrder .= ', ';
                }
                if($value > 0) {
                    $strOrder .= "`{$field}` DESC";
                } else {
                    $strOrder .= "`{$field}` ASC";    
                }
            }
            if($strOrder != '') {
                $strOrder = 'ORDER BY ' . $strOrder;
            }
        }
        return $strOrder;
    }
    
    /**
    * Построить строку лимита для запроса
    * 
    * @param int | array $limit - сколько записей выбрать, если array('start'=>(int), 'count' =>(int))
    * 
    * @return  str
    */
    protected function buildLimit($limit = 0)
    {
        $result = '';
        if(is_array($limit) && count($limit) > 0) {
            if(isset($limit['start']) && $limit['start'] > 0) {
                if(isset($limit['count']) && $limit['count'] > 0){
                    $result = "LIMIT {$limit['start']}, {$limit['count']}";
                } else {
                    $result = "LIMIT {$limit['start']}, 1";     
                }        
            } else if(isset($limit['count']) && $limit['count'] > 0){
                $result = "LIMIT {$limit['count']}";    
            }
        } else {
            $limit = (int)$limit;
            $result = ($limit > 0)? "LIMIT {$limit}":'';
        }
        return $result;
    }
    
    /**
    * Выборка строк из таблицы db
    * 
    * @param string $table
    * @param array $where as array($field => $value, ...)
    * @param array $order as array($field => 0 | 1, ...)   0 - ASC, 1 - DESC
    * @param int | array $limit - сколько записей выбрать, если array('start'=>(int), 'count' =>(int))
    * 
    * @return  array массив записей с полями, соответствующими табличным
    */
    protected function select($table, $where = false, $order = false, $limit = 0) 
    {
        $result = false;
        if($table != '') {
            if(is_array($where)) {
                $strWhere = $this->buildWhere($where, 'AND', true); 
            } else { $strWhere = ''; }
            $strOrder = $this->buildOrder($order);
            $strLimit = $this->buildLimit($limit);
            
            $this->_query = "SELECT * FROM `{$table}` {$strWhere} {$strOrder} {$strLimit}"; 
            
            if($limit == 1) {
                $result = $this->_stream->super_query($this->_query);
            } else {
                $result = $this->_stream->query_fetch($this->_query);  
            }
        }
        return $result;
    }
    
    /**
    * Выборка строк из таблицы db
    * Cо свободным $where ввиде строки
    * 
    * @param string $table
    * @param string $where 
    * @param array $order as array($field => 0 | 1, ...)   0 - ASC, 1 - DESC
    * @param int | array $limit - сколько записей выбрать, если array('start'=>(int), 'count' =>(int))
    * 
    * @return  array массив записей с полями, соответствующими табличным
    */
    protected function selectFreeWhere($table, $where, $order = false, $limit = 0) 
    {
        $result = false;
        
        if($limit == 1) {
            $superQuery = true;
        } else {
            $superQuery = false;
        }
        if($table != '') {
            if(is_array($where)){
                $strWhere = $this->buildWhere($where, 'AND', false);
            } else {
                $strWhere = $where; 
            }
            $strOrder = $this->buildOrder($order);
            $strLimit = $this->buildLimit($limit);
            
            if($strWhere != '') {
                $this->_query = "SELECT * FROM `{$table}` WHERE {$strWhere} {$strOrder} {$strLimit}";
                if($superQuery) {
                    $result = $this->_stream->super_query($this->_query);    
                } else {
                    $result = $this->_stream->query_fetch($this->_query);
                }
            }
        }
        return $result;
    }
    
    /**
    * Построение строки WHERE 
    * 
    * @param array $where as array($field => $value, ...) 
    * @param str $method  as IN, AND, OR
    * @param bool $fl_full полная строка (WHERE ...) true
    * @return string
    */
    protected function buildWhere($where, $method = 'AND', $fl_full = true)
    {
        $result = '';
        switch($method) {
            case 'OR':
                $method = 'OR';
            break;
            case 'IN':
                $method = 'IN';
            default:
                $method = 'AND';
        }
        if($where && is_array($where) && count($where) > 0) {
            foreach($where as $field => $value) {
                if($method == 'IN') {
                    if(!is_null($value) && is_array($value) && count($value) > 0) {
                        $strIn = '';
                        foreach($value as $item) {
                            if($strIn != '') {
                                $strIn .= ', ';
                            }
                            $strIn .= "'{$value}'";      
                        }
                        if($strIn != '') {
                            if($result != '') {
                                $result .= ' AND ';
                            } 
                
                            $result .= "`{$field}` IN({$strIn})"; 
                        } 
                    } else if(!is_null($value)) {
                        if($result != '') {
                            $result .= " AND ";
                        } 
                        $result .= "`{$field}` = '{$strIn}'";
                    } else {
                        $result .= "`{$field}` IS NULL";    
                    } 
                } else {
                    if($result != '') {
                        $result .= " {$method} ";
                    } 
                    if(!is_null($value)) {
                        $result .= "`{$field}` = '{$value}'";
                    } else {
                        $result .= "`{$field}` IS NULL";     
                    }    
                }
            }
        
            if($fl_full) {
                $result = "WHERE {$result}";
            }
        }
        return $result;
    }
    
    /**
    * Выборка одной строки из таблицы db,
    * Частный случай для select
    * 
    * @param string $table
    * @param array $where as array($field => $value, ...)
    * 
    * @return  array  - строка таблицы в виде массива
    */
    protected function getRow($table, $where) 
    {
        return $this->select($table, $where, false, 1);   
    }
    
    abstract function save();
    abstract function del();
    
    protected function timeout($sec = 5, $info = false)
    {
        $sec = (int)$sec;
        $now = time();
        if($sec > 0 && $sec < 31) {
            $do = true;
            while($do) {
                if((time() - $now) >= $sec) {
                    $do = false;
                }
            }
        }
        if($info) {
            echo "\r\ntimeout {$sec} сек.\r\n";
        }
        return true;
    }
    
}  
?>
