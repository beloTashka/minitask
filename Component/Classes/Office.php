<?php
namespace Component;
/**
* Абстрактный класс для управления данными и сущностями
* 
* @name Component\Office
*/
abstract class Office 
{   
    use TraitUserQuery;
    use Trait404;
    use TraitDisplay; 
     
    /**
    * Возвращает имя tpl шаблона
    * Соответствующего запросу пользователя
    * @return str
    */
    abstract public function getTplName();
    
    /**
    * Получить ассоциированный массив данных для вывода в шаблоне
    * соответствующем запросу пользователя
    *
    * @return array
    */
    abstract public function getDataView(); 
   
}  
