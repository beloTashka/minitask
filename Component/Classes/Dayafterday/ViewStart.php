<?php
namespace Component\Dayafterday;  

/**
*  Класс для визуализации компонета Комментарии
* @name \Component\Dayafterday\ViewStart  
*/

class ViewStart extends \Component\ViewExtVer01
{
    protected $mod = 'Dayafterday'; 
//    use \Component\TraitDisplay; 
    public function __construct($nameTpl = '') 
    {   
        $this->echo = true; 
        $this->initTpls($nameTpl);
        
        $this->initJs();
        $this->initCss();
        
        return $this;
    }
    
    private function initTpls($nameTpl)
    {
        $this->dirTpl = $this->rootDir . 'Component/Layouts/' . $this->mod . '/tpl/';
        $this->dirJs = $this->serv . 'Component/Layouts/' . $this->mod . '/js/';
        $this->dirCss = $this->serv . 'Component/Layouts/' . $this->mod . '/css/';
        
        if($nameTpl === '') {
            $nameTpl = 'start';    
        }

        $this->pushTpl($nameTpl); 
         
        return $this;
    }
    
    
    
    private function initCss() 
    {
        $this->css = array();
        $this->css[] = $this->dirCss . 'style.css';
        return $this->js;
    }
    
    private function initJs() 
    {
        $this->js = array();
        $this->js[] = $this->dirJs . 'index.js';
        return $this->js;
    }
    
    /**
     * Добавить файл js в массив для подключения
     *
     * @param str $fileName
     */
    public function addJs($fileName) 
    {
        if ($fileName != '') {
            $this->js[] = '/' . $this->mod . '/js/' . $fileName . '.js'; 
        }
        return $this->js;
    }
}
