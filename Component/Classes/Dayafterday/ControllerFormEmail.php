<?php
namespace Component\Dayafterday;
 
/**
* Управление Запросами / Ответами 
* через главную страницу проекта Dayafterday
* @name \Component\Dayafterday\ControllerFormEmai 
*/
class ControllerFormEmail extends \Component\ControllerPageUnit {        

    /**
    * Заполняет необходимые для проверки
    *  $controlLvl;
    *  $controlRequest;
    */
    public function __construct($lvl, $requestUrl, $requestData)
    {
        $this->echo = true;
        $this->init($lvl, $requestUrl, $requestData); 
        
        $this->_dataOffice = new OfficeFormEmail($lvl, $requestUrl, $requestData); 
        $nameTpls = $this->_dataOffice->getTplsName();

        $this->_view = new ViewFormEmail();
        $this->_view->pushTpls($nameTpls); 
        
        return $this;
    } 
    
    public function handler()
    {
        if($this->_dataOffice->isDataForm() && !$this->_dataOffice->isError()) {
            // отправить сообщение
            $this->_dataOffice->sendEmail();
        }
        return $this;
    }


}   

