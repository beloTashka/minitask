<?php
namespace Component\Dayafterday;  


/**
*  Класс описания сущности Дата
* @name \Component\Dayafterday\EntityFormAccess
*/

class EntityFormAccess extends \Component\EntityItem
{
    protected $_login;
    protected $_pass;
    
    public function __construct() 
    {
        $this->_login = new \Component\EntityFieldByForm('login', 'login', 'text', 3, 15, 'login', true);
        $this->_pass = new \Component\EntityFieldByForm('pass', 'pass', 'password', 8, 100, 'password', true);
        return $this;
    }
    
    public function set__login($value)
    {
        $this->_login->__set('value', $value);
        return $this;    
    }
    
    public function set__pass($value)
    {
        $this->_pass->__set('value', $value);
        return $this;    
    }
}
