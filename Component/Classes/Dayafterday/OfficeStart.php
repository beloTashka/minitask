<?php
namespace Component\Dayafterday; 
/**
* Абстрактный класс для управления данными и сущностями
* 
* @name Component\Office
*/
class OfficeStart extends \Component\Office
{   
    private $access = true;
    
    public function __construct($lvl, $requestUrl, $requestData) 
    {
        $this->echo = true;
        $this->init($lvl, $requestUrl, $requestData);
        $this->setTpl404($this->requestData); 
        return $this;
    }
    
    public function checkAccess($group)
    {
        if($this->lvl > 0) {
            if($group) {
                $this->access = true; 
            } else {
                $this->access = false;
            }
        }
        return $this->access;
    }
    
    /**
    * Возвращает имя tpl шаблона
    * Соответствующего запросу пользователя
    * @return array()
    */
    public function getTplsName()
    {
        $result = array();
        if ($this->lvl === 0) {
            // logo без ссылки
            $result[] = 'start_main';
        } else {
            // простой контент с использованием <alias>.tpl as $content
            $result[] = 'start'; 
            if ($this->access) {
                $result[] = $this->requestUrl[$this->lvl]; 
            } else {
                $result[] = 'access';    
            }
        }
        
        $tpl404 = $this->getTpl404();
        if ($tpl404) {
            $result[] = $tpl404;    
        }
        return $result;   
    }
    
    /**
    * Возвращает имя tpl шаблона
    * Соответствующего запросу пользователя
    * @return str
    */
    public function getTplName()
    {
        $result = '';
        if($this->lvl === 0) {
            // logo без ссылки
            $result = 'start_main';
        } else {
            // простой контент с использованием <alias>.tpl as $content
            $result = 'start'; 
 
        }
        return $result;   
    }
    
    public function getTplContent()
    {
        $result = $this->getTpl404();
        if (!$result) {
            if ($this->access) {
                $result = $this->requestUrl[$this->lvl]; 
            } else {
                $result = 'access';    
            }
        }
        return $result;   
    }
    
    /**
    * Получить ассоциированный массив данных для вывода в шаблоне
    * соответствующем запросу пользователя
    *
    * @return array
    */
    public function getDataView()
    {
        $result = array();
        $result['title'] = 'Разработка сайтов. Full-stack web developer. PHP программист';
        $result['description'] = 'Разработка и сопровождение сайтов на PHP';
        $result['keywords'] = 'Программист PHP MySql JavaScript jQuery';
        $result['type_page'] = 'WebPage';
        $error404 = $this->getTpl404();
        if ($error404) {
            $result['title'] = 'Страница не найдена';
            $result['description'] = 'error 404. Page not found';
            $result['keywords'] = '404';
        }
        return $result;    
    } 
   
}  
