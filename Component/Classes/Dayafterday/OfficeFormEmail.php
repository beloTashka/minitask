<?php
namespace Component\Dayafterday;  


/**
*  
* @name \Component\Dayafterday\OfficeFormEmail
*/

class OfficeFormEmail extends \Component\Office 
{
    private $_formEmail;
    
    public function __construct($lvl, $requestUrl, $requestData) 
    {
        $this->echo = true;
        
        $this->init($lvl, $requestUrl, $requestData); 
        $this->_formEmail = new EntityFormEmail();
        $this->fillUpFormEmail();
        
        return $this;
    }
    
    /**
    * Дает ответ на вопрос была ли попытка отправки сообщения
    * @return bool
    */
    public function isDataForm()
    {
        $result = false;
        $fields = array();
        $fieldsForm = $this->_formEmail->fieldsAsArray();
        foreach ($fieldsForm as $field) {
            if(mb_strpos($field, '_') === 0) {
                $name = mb_substr($field, 1);
            } else {
                $name = $field;
            }
            if (isset($this->requestData[$name]) && $this->requestData[$name] != '') {
                $fields[$name] = $this->requestData[$name];    
            } 
        } 
        if (count($fields) > 0) {
            $result = true;
        }
        return $result;
    }
    
    public function isError()
    {
        $result = false;
        $fields = $this->_formEmail->toArray(); 
        foreach ($fields  as $field => $attr) {
            if(isset($attr['error']) && $attr['error'] && !$result) {
                $result = true;
            }
        }
        return $result;
    }
    
    public function sendEmail()
    {
        $message = $this->getMessage();
//        $this->display($message);
        
        @mail("work.natawhite@gmail.com", "message from site", $message,
            "From: webmaster@test\r\n"
            ."X-Mailer: PHP/" . phpversion());
        
        return $this;
    }
    
    /**
    * Полный текст сообщения
    */
    private function getMessage()
    {      
        $result = $this->_formEmail->__get('_message')->__get('value');
        $result .= "\r\n\r\n";
        $result .= 'Сообщение отправил: '; 
        $result .= $this->_formEmail->__get('_name')->__get('value'); 
        $result .= "\r\n\r\n"; 
        $result .= $this->_formEmail->__get('_email')->__get('value'); 
        return $result;   
    }
    
    private function fillUpFormEmail()
    {
        $fields = array();
        $fieldsForm = $this->_formEmail->fieldsAsArray();
        foreach ($fieldsForm as $field) {
            if(mb_strpos($field, '_') === 0) {
                $name = mb_substr($field, 1);
            } else {
                $name = $field;
            }
            if (isset($this->requestData[$name]) && $this->requestData[$name] != '') {
                $fields[$name] = $this->requestData[$name];    
            } 
        } 
        if (count($fields) > 0) {
            $this->setFormEmail($fields);
        }
        return $this; 
    }
    
    public function setFormEmail($fields)
    {
        $fieldsForm = $this->_formEmail->fieldsAsArray();
        foreach ($fieldsForm as $field) {
            if(mb_strpos($field, '_') === 0) {
                $name = mb_substr($field, 1);
            } else {
                $name = $field;
            }
            if(isset($fields[$name])) {
                $value = $fields[$name];
            } else {
                $value = '';
            }
            $this->_formEmail->__set($field, $value);
        }
        return $this;
    }
    
    /**
    * Возвращает имя tpl шаблона
    * Соответствующего запросу пользователя
    * @return array()
    */
    public function getTplsName()
    {
        $result = array();
         
        // простой контент с использованием <alias>.tpl as $content
        $result[] = $this->getTplName(); 
        $result[] = $this->requestUrl[$this->lvl]; 
        $result[] = 'field_input'; 
        $result[] = 'field_textarea'; 
 
        return $result;   
    }
    
    /**
    * Возвращает имя tpl шаблона
    * Соответствующего запросу пользователя
    * @return str
    */
    public function getTplName()
    {
        $result = '';
         
        if($this->lvl === 0) {
            // logo без ссылки
            $result = 'start_main';
        } else {
            // простой контент с использованием <alias>.tpl as $content
            $result = 'start'; 
 
        }
        return $result;   
    }
    
    public function getTplContent()
    {
        return $this->requestUrl[$this->lvl];   
    }
    
    /**
    * Получить ассоциированный массив данных для вывода в шаблоне
    * соответствующем запросу пользователя
    *
    * @return array
    */
    public function getDataView()
    {
        $result = array();
        $result['title'] = 'Контакты. Обсудить сотрудничество.';
        $result['description'] = 'Обсудить проект. Помощь в написании ТЗ сайта.';
        $result['keywords'] = 'Новый Проект Сайт Написать ТЗ';
        $result['type_page'] = 'WebPage';
        
        $result['field'] = $this->_formEmail->toArray();
        $errorMessage = '';
        foreach ($result['field']  as $field => $attr) {
            $result['field'][$field]['class_error'] = $this->getClassError($attr);
            $errorMessage = $this->descrErrors($attr, $errorMessage);
        }
        if ($errorMessage != '') {
            $result['class_error'] = 'error';    
            $result['error_message'] = $errorMessage;    
        }  else if(isset($this->requestData['message']) && $this->requestData['message'] != '') {
            $result['green_message'] = 'Сообщение отправлено';
            
            if(isset($result['field']['_message']['value'])) {
                $result['field']['_message']['value'] = '';
            }
        } 
        $result['class_hide_message'] = 'hide'; 
        return $result;    
    } 
    
    /**
    * Получение класса для поля не прошедшего валидацию
    * @param array $attr свойства поля
    * @return str
    */
    private function getClassError($attr) 
    {
        $result = '';
        if (isset($attr['error']) && $attr['error']) {
            $result = 'error';
        }
        return $result;
        
    }
    
    /**
    * Получение Сообщения об ошибке для поля не прошедшего валидацию
    * @param array $attr свойства поля
    * @param str $result описания ошибок уже проверенных полей
    * @return str
    */
    private function descrErrors($attr, $result = '') 
    {
        if ($attr['error']) {
            $message = ''; 
            switch($attr['name']) {
                case 'email':
                    $message = 'Заполните поле email.';     
                    break;
                case 'name':
                    $message = 'Укажите Ваше имя, пожалуйста.';     
                    break;
                case 'message':
                    if($attr['error'] == 2) {
                        $message = 'Сообщение слишком короткое, расскажите о задаче подробнее';     
                    } else if($attr['error'] == 1) {
                        $message = 'Расскажите о задаче подробнее, невозможно отправить пустое сообщение';
                    }
                    break;
                } 
               $result .= ($result != '' && $message != '')? ' | ':'';
               $result .= $message;     
        } 
       return $result; 
    }
    
}
