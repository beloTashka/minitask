<?php
namespace Component\Dayafterday;
use Component;
use Component\Users as Users;
 
/**
* Управление Запросами / Ответами 
* через главную страницу проекта Dayafterday
* @name \Component\Dayafterday\ControllerStart 
*/
class ControllerPass extends \Component\ControllerPageUnit { 
     private $_officeUser;

    /**
    * Заполняет необходимые для проверки
    *  $controlLvl;
    *  $controlRequest;
    */
    public function __construct($lvl, $requestUrl, $requestData)
    {
        $this->echo = true;
        $this->init($lvl, $requestUrl, $requestData);
        $this->_officeUser = new Users\OfficeUser($this->requestData);
        
        $this->_dataOffice = new OfficeStart($this->lvl, $this->requestUrl, $this->requestData); 
        $this->_dataOffice->checkAccess($this->_officeUser->groupAccess());
        $nameTpls = $this->_dataOffice->getTplsName();

        $this->_view = new ViewStart();
        $this->_view->pushTpls($nameTpls);  
        
        return $this;
    } 
    
    public function handler()
    {
        return $this;
    }

}   

