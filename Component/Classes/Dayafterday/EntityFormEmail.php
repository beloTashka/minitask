<?php
namespace Component\Dayafterday;  


/**
*  Класс описания сущности Дата
* @name \Component\Dayafterday\EntityFormEmail
*/

class EntityFormEmail extends \Component\EntityItem
{
    protected $_name;
    protected $_email;
    protected $_phone;
    protected $_message;
     
    public function __construct() 
    {
        $this->_name = new \Component\EntityFieldByForm('name', 'Имя', 'text', 3, 50, false, true);
        $this->_email = new \Component\EntityFieldByForm('email', 'Email', 'email', 8, 100, false, true);
        $this->_phone = new \Component\EntityFieldByForm('phone', 'Телефон','tel', false, 18, false);
        $this->_message = new \Component\EntityFieldByForm('message',  'Сообщение','text', 10, 1000, 'textarea', true);
        
        return $this;   
    }
    
    public function set__name($value)
    {
        $this->_name->__set('value', $value);
        return $this;    
    }
    
    public function set__email($value)
    {
        $this->_email->__set('value', $value);
        return $this;    
    }
    
    public function set__phone($value)
    {
        $this->_phone->__set('value', $value);
        return $this;    
    }
    
    public function set__message($value)
    {
        $this->_message->__set('value', $value);
        return $this;    
    }
}
