<?php
namespace Component\Dayafterday;
 
/**
* Управление Запросами / Ответами 
* через главную страницу проекта Dayafterday
* @name \Component\Dayafterday\ControllerDays 
*/
class ControllerDays extends \Component\ControllerPageUnit { 

    /**
    * Заполняет необходимые для проверки
    *  $controlLvl;
    *  $controlRequest;
    */
    public function __construct($lvl, $requestUrl, $requestData)
    {
        $this->init($lvl, $requestUrl, $requestData);
        return $this;
    } 
    
    public function handler()
    {
        return $this;
    } 
    
}   

