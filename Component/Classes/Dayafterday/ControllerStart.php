<?php
namespace Component\Dayafterday;
 
/**
* Управление Запросами / Ответами 
* через главную страницу проекта Dayafterday
* @name \Component\Dayafterday\ControllerStart 
*/
class ControllerStart extends \Component\ControllerPageUnit { 
    

    /**
    * Заполняет необходимые для проверки
    *  $controlLvl;
    *  $controlRequest;
    */
    public function __construct($lvl, $requestUrl, $requestData)
    {
        $this->echo = true;
        $this->init($lvl, $requestUrl, $requestData);  
        $this->_dataOffice = new OfficeStart($this->lvl, $this->requestUrl, $this->requestData); 
        $nameTpls = $this->_dataOffice->getTplsName();

        $this->_view = new ViewStart();
        $this->_view->pushTpls($nameTpls);  
        
        return $this;
    } 
    
    public function handler()
    {
        return $this;
    }

}   

