<?php
namespace Component; 


/**
*  Класс описания сущности Дата
* @name \Component\Date
*/

class EntityDate extends EntityItem
{
    private $date;
     
    public function __construct($date = null) 
    {
         if(!is_null($date)){
            $type = gettype($date);
            if($type != 'integer') {
                $date_1 = $this->parseDate($date);
                if(!$date_1) {
                    $date = (int)$date;    
                } else {
                    $date = $date_1;
                }    
            } else if($date < 0) {
                $date = abs($date);
            }
           
        } 
        
        if(is_null($date) || $date === false) {
            $date = time();    
        }
        $this->set_date($date);
        
        return $this;   
    }
    

    
    // Заполнение даты

    public function set_date($value) 
    {
        $this->date = $value;
        return $this;   
    }

   // Получение даты
    
    public function get_date()
    {
        return $this->date;
    }
    
    /**
    * Номер месяца текущей даты 
    */
    public function getMounth()
    {
        $result = date('n', $this->date); 
        return $result;
    }
    
    /**
    * Год даты
    */
    public function getYear()
    {
        $result = date('Y', $this->date); 
        return $result;
    }
    
    /**
    * День месяца с ведущим 0
    */
    public function getDay()
    {
        $result = date('d', $this->date); 
        return $result;
    } 
    
    public function getDateFormat($format = 'd.m.Y')
    {
        $result = date($format, $this->date); 
        return $result;
        
    } 
    
        /**
    * преобразование форматированной строки даты в формат unix
    *
    * @param str $sdate
    * @return int
    */
    public function parseDate($sdate)
    {
        $result = false;
        if (isset($sdate) && $sdate != '') {
            preg_match('|^(\d{2})\-(\d{2})\-(\d{4})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch1);
            preg_match('|^(\d{2})\/(\d{2})\/(\d{4})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch2);
            preg_match('|^(\d{2})\.(\d{2})\.(\d{4})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch3);

            if (count($mtch1) == 7 || count($mtch2) == 7 || count($mtch3) == 7) {
                if (count($mtch1) == 7) $mtch = $mtch1;
                if (count($mtch2) == 7) $mtch = $mtch2;
                if (count($mtch3) == 7) $mtch = $mtch3;
                $result = mktime($mtch[4], $mtch[5], $mtch[6], $mtch[2], $mtch[1], $mtch[3]);
            } else {
                preg_match('|^(\d{4})\-(\d{2})\-(\d{2})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch1);
                preg_match('|^(\d{4})\/(\d{2})\/(\d{2})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch2);
                preg_match('|^(\d{4})\.(\d{2})\.(\d{2})\s(\d{2}):(\d{2}):(\d{2})$|', trim($sdate), $mtch3);

                if (count($mtch1) == 7 || count($mtch2) == 7 || count($mtch3) == 7) {
                    if (count($mtch1) == 7) $mtch = $mtch1;
                    if (count($mtch2) == 7) $mtch = $mtch2;
                    if (count($mtch3) == 7) $mtch = $mtch3;
                    $result = mktime($mtch[4], $mtch[5], $mtch[6], $mtch[2], $mtch[3], $mtch[1]);
                } else {
                    preg_match('|^(\d{2})\-(\d{2})\-(\d{4})$|', trim($sdate), $mtch1);
                    preg_match('|^(\d{2})\/(\d{2})\/(\d{4})$|', trim($sdate), $mtch2);
                    preg_match('|^(\d{2})\.(\d{2})\.(\d{4})$|', trim($sdate), $mtch3);
                    if (count($mtch1) == 4 || count($mtch2) == 4 || count($mtch3) == 4) {
                        if (count($mtch1) == 4) $mtch = $mtch1;
                        if (count($mtch2) == 4) $mtch = $mtch2;
                        if (count($mtch3) == 4) $mtch = $mtch3;
                        $result = mktime(0, 0, 0, $mtch[2], $mtch[1], $mtch[3]);
                    } else {
                        preg_match('|^(\d{4})\-(\d{2})\-(\d{2})$|', trim($sdate), $mtch1);
                        preg_match('|^(\d{4})\/(\d{2})\/(\d{2})$|', trim($sdate), $mtch2);
                        preg_match('|^(\d{4})\.(\d{2})\.(\d{2})$|', trim($sdate), $mtch3);
                        if (count($mtch1) == 4 || count($mtch2) == 4 || count($mtch3) == 4) {
                            if (count($mtch1) == 4) $mtch = $mtch1;
                            if (count($mtch2) == 4) $mtch = $mtch2;
                            if (count($mtch3) == 4) $mtch = $mtch3;
                            $result = mktime(0, 0, 0, $mtch[2], $mtch[3], $mtch[1]);
                        }
                    }
                }
            }
        }
        return $result;
    }
}
