<?php
namespace Component;
/**
* Абстрактный класс для работы визуализации сущеностей/данных
* Расширенный внесением шаблонов и получением html
* 
* @author Nataliie Bilotzerkovska <work.natawhite@gmail.com>
* @copyright 2020 Nataliie Bilotzerkovska
* @license GPU
* @license https://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
* @name Component\ViewExtVer01
*/
class ViewExtVer01 extends View 
{
    protected $mod;
    protected $rootDir = ROOT_DIR;
    protected $serv = MYDOMEN; 
    protected $dirTpl;  // папка с шаблонами 
    protected $dirJs;  // папка с файлами js 
    protected $dirCss;  // папка с файлами css 
    
    protected $content = ''; // шаблон .tpl
    
    protected $js;
    protected $css;
    
    protected $vTpls; 
    
    use TraitDisplay;
    use TraitPars;
    
    protected function buildCss()
    {
        $result = '';
        foreach($this->css as $url) {
            $result .= '<link rel="stylesheet" type="text/css" href="' . $url . '">';
        }
        return $result;
    }
    
    /**
    * Создание блока js в head html
    * @return html
    */
    protected function buildJs()
    {
        $result = '';
        foreach($this->js as $url) {
            $result .= '<script src="' . $url . '"></script>';
        }
        return $result;
    }
    
    public function pushTpls($arrNames) 
    {
        if (is_array($arrNames) && count($arrNames) > 0) {
            foreach ($arrNames as $name) {
                if($name && $name != '') {
                    $this->pushTpl($name);
                }
            }   
        }
        return $this;
    }
    
    /**
    * Заполнение массива шшаблонов
    * в формате массива
    * array($name => array('url' => str
    * , 'var' => array($tag1, $tag2, ...)  Переменные, которые необходимо заполнить при отображении
    * , 'tpl' => array($fileName1, $filename2, ...))) Шаблоны, которые необходимо подтянуть
    * 
    * @param str $name
    * @param str $url
    */
    protected function pushTpl($name)
    { 
        if (!is_array($this->vTpls)) {
            $this->vTpls = array();
        }  
        if (!isset($this->vTpls[$name])) {
            $this->vTpls[$name] = array();
        
            $url = $this->dirTpl . $name . '.tpl';
            if ($name != '' && file_exists($url) && !isset($this->vTpls[$name]['url'])) {
                $html = @file_get_contents($url);
                if ($html && $html != '') {
                    $this->vTpls[$name]['url'] = $url;
                    $this->vTpls[$name]['var'] = $this->getVars($html);  // переменные
                    $this->vTpls[$name]['tpl'] = $this->getTPLsName($html); // вызываемые шаблоны
                    $this->vTpls[$name]['html'] = $html;
                       
                    if (is_array($this->vTpls[$name]['tpl']) 
                        && count($this->vTpls[$name]['tpl']) > 0) { 
        
                        foreach($this->vTpls[$name]['tpl'] as $tplName) {
                            $url = $this->dirTpl . $tplName . '.tpl'; 
                            if(!isset($this->vTpls[$tplName]) && file_exists($url)) { 
                                $this->pushTpl($tplName); 
                            }   
                        } 
                    }
                } 
            }
        }
        
        return $this;
    }
    
    /** 
    * Получение массива переменных из шаблона
    */
    protected function getVars($html) 
    {
        $result = array();
        if (!is_null($html) && $html) {
            $var = false;
            $this->initSlipText('{$', '}', $html);
            do {
                $var = $this->getNextCut();
                if($var && $var != '') {
                    $result[] = $var;    
                }
            } while ($this->slip !== false && $var !== false);
        }
        return $result;  
    }
    
    /** 
    * Получение массива шаблонов из шаблона
    */
    protected function getTPLsName($html) 
    {
        $result = array();
        if (!is_null($html) && $html) {
            $var = false;
            $this->initSlipText('{include ', '.tpl}', $html);
            do {
                $var = $this->getNextCut();
                if($var && $var != '') {
                    $result[] = $var;    
                }
            } while ($this->slip !== false && $var !== false);
        }
        return $result;  
    }
    
    /**
    * Получить html шаблон из массива шшаблонов в классе
    * @param str $key
    * @param str $type
    * @return html
    */
    protected function getTplHTML($name, $result = '')
    {
        // html включенных шаблонов - вся иерархия 
        
        if($name && $name != '' && isset($this->vTpls[$name]) 
            && isset($this->vTpls[$name]['url']) 
            && (!isset($this->vTpls[$name]['html']) 
            || $this->vTpls[$name]['html'] == '')) {
            $this->vTpls[$name]['html'] = @file_get_contents($this->vTpls[$name]['url']);
        }  
        if(isset($this->vTpls[$name]['html']) 
            && $this->vTpls[$name]['html'] 
            && $this->vTpls[$name]['html'] != '') {
            $html = $this->vTpls[$name]['html']; 
        } else {
            $html = '';
        }
       
        
        if($result == '') {
            if(isset($this->vTpls[$name]['html'])) {
                $result = $this->vTpls[$name]['html'];
            }
        } else {
            $result = str_replace('{include ' . $name . '.tpl}', $html, $result);
        }
            
        $tplSearch = $this->getSearchTpl($name);
        if($tplSearch && is_array($tplSearch) && count($tplSearch) > 0) { 
            foreach($this->vTpls[$name]['tpl'] as $tpl) {
                $result = $this->getTplHTML($tpl, $result); 
            }
        }
         
         return $result;
    }
    
    /**
    * Получить массив шаблонов, использумых в html $name
    * первый уровень иерархии
    * для замены на их html код
    * 
    * @param str $name 
    * @return array('{include *.tpl}', '{include *.tpl}', ...) 
    */
    protected function getSearchTpl($name)
    {
        $result = array();
        
        if(isset($this->vTpls[$name]['tpl']) 
            && is_array($this->vTpls[$name]['tpl'])
            && count($this->vTpls[$name]['tpl']) > 0) {
            foreach($this->vTpls[$name]['tpl'] as $var) {
                $result[] = '{include ' . $var . '.tpl}';
            }
        } 
       
        return $result;
    }
    
    /**
    * Получить массив параметров всех используемых переменных
    * 
    * @param str $name 
    * @return array() | false
    */
    protected function getSearchVar($name)
    {
        $result = array();
        $tpls = $this->getSubTplNameSearch($name); 
        if(!in_array($name, $tpls)) {
            $tpls[] = $name;
        }
        
        foreach($tpls as $nameTpl) {
            if(isset($this->vTpls[$nameTpl]['var']) 
                && is_array($this->vTpls[$nameTpl]['var'])
                && count($this->vTpls[$nameTpl]['var']) > 0) {
                foreach($this->vTpls[$nameTpl]['var'] as $var) {
                    $result[] = '{$' . $var . '}';
                }
            } 
        } 
        return $result;
    }
    
    /**
    * Получение всех переменных включенных шаблонов
    */
    protected function getSubTplVarSearch($name, $result = false)
    {
        if (!$result || !is_array($result)) {
            $result = array();
        } 
        $except =  '{include ' . $name . '.tpl}';
        
        if(isset($this->vTpls[$name]['tpl']) 
            && is_array($this->vTpls[$name]['tpl'])
            && count($this->vTpls[$name]['tpl']) > 0) {
            foreach($this->vTpls[$name]['tpl'] as $tpl) {
                $str = '{include ' . $tpl . '.tpl}';
                if (!in_array($str, $result) && $str != $except) {
                    $result[] = $str;
                    $result = $this->getSubTplVarSearch($tpl, $result);
                }
            }
        } 
         
        return $result;
    }
    
    /**
    * Получение всех имен включенных шаблонов
    */
    protected function getSubTplNameSearch($name, $result = false)
    {
        if (!$result || !is_array($result)) {
            $result = array();
        } 
        
        if(isset($this->vTpls[$name]['tpl']) 
            && is_array($this->vTpls[$name]['tpl'])
            && count($this->vTpls[$name]['tpl']) > 0) {
            foreach($this->vTpls[$name]['tpl'] as $tpl) {
                if ($tpl != '' && !in_array($tpl, $result) && $tpl != $name) {
                    $result[] = $tpl;
                    $result = $this->getSubTplNameSearch($tpl, $result);
                }
            }
        } 
         
        return $result;
    }
    
    
    /**
    * Получить html из шаблона
    * @param str $name - имя шаблона 
    * @param array $arrValues массив значений для замены array('{param1}' => val, '{param2}' =>val)
    * 
    * @return html
    */
    public function makeHTML($name, $arrValues)
    {

        $result = $this->getTplHTML($name);
        
        $varSearch = $this->getSearchVar($name);
        
        if (isset($arrValues['field']) 
            && is_array($arrValues['field'])
            && count($arrValues['field']) > 0) {
            foreach ($arrValues['field'] as $field => $attr) {
                if(in_array('{$field' . $field .'}', $varSearch)) {
                    $arrValues['field' . $field] = $this->makeHTML('field_' . $attr['inForm'], $attr);
                }    
            }
        }
 
        $result = $this->replaceVar($varSearch, $arrValues, $result); 
        return $result;
    } 
    
    private function replaceVar($varSearch, $arrValues, $html)
    {
        $varReplace = array();
        $values = array();
        if (isset($arrValues) && is_array($arrValues) && count($arrValues) > 0) {
            foreach ($arrValues as $key => $val) {
                if(!is_array($val)) {
                    $values['{$' . $key . '}'] = $val;
                }
            }
        }
        
        if (isset($varSearch) && is_array($varSearch) && count($varSearch) > 0) {
            foreach ($varSearch as $param) {
                $varReplace[] = (isset($values[$param]))? $values[$param]: '';
            }
        } 

        $result = str_replace($varSearch, $varReplace, $html); 
        return $result;
        
    }
    
    /**
    * Построение простого select
    * @param array $dataSelect  as array($value => $descr, ...)
    * @param array $default as array('value' => $value, 'descr' => $descr)
    */
    protected function makeSimpleSelect($dataSelect, $name = '', $id = '', $default = array())
    {
        $result = '<select'; 
        if($name != '') {
            $result .= ' name = "' .$name .'"';    
        }
        if($id != '') {
            $result .= ' id="' .$id .'"';    
        }
        $result .= '>';
        
        $result .= $this->makeOptionSelect($dataSelect, $default);

        $result .= '</select>';
        
        return $result;
    }
    /**
    * Построение option для select
    * @param array $dataSelect  as array($value => $descr, ...)
    * @param array $default as array('value' => $value, 'descr' => $descr)
    */
    protected function makeOptionSelect($dataSelect, $default = array())
    {
        $result = '';
        
        if(is_array($dataSelect) && count($dataSelect) > 0) {
            foreach($dataSelect as $value => $descr) {
                if(!isset($default['value']) || !isset($dataSelect[$default['value']]) || $default['value'] != $value) {
                    $result .= '<option value="' . $value . '">';
                    $result .= $descr;
                    $result .= '</option>';
                } else if($default['value'] == $value) {
                    $result .= '<option value="' . $default['value'] . '" selected>';
                    $result .= $default['descr'];
                    $result .= '</option>';    
                }
            }    
        }
        return $result;
    }
    
        /**
    * Построение option для select
    * @param array $dataSelect  as array($value => $descr, ...)
    * @param array $default as array($value, $value, ...)
    */
    protected function makeOptionMultiSelect($dataSelect, $default = array())
    {
        $result = '';
        if(!is_array($default) || count($default) == 0) {
            $default = array('all', 0);
        }
        
        if(is_array($dataSelect) && count($dataSelect) > 0) {
            foreach($dataSelect as $value => $descr) {
                if(!in_array($value, $default)) {
                    $result .= '<option value="' . $value . '">';
                    $result .= $descr;
                    $result .= '</option>';
                } else {
                    $result .= '<option value="' . $value . '" selected>';
                    $result .= $descr;
                    $result .= '</option>';    
                }
            }    
        }
        return $result;
    }
}  
