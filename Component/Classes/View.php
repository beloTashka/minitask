<?php
namespace Component;
/**
* Абстрактный класс для работы визуализации сущеностей/данных
* 
* @name Component\View
*/
abstract class View 
{     
    /**
    * Заполнение массива шшаблонов
    * в формате массива
    * array($name => array('url' => str, 'search' => array($tag1, $tag2, ...)))
    * 
    * @param str $name
    * @param str $url
    * @param array $search
    */
    abstract protected function pushTpl($name);
    
    /**
    * Получить html шаблон из массива шшаблонов в классе
    * @param str $key
    * @param str $type
    * @return html
    */
    abstract protected function getTplHTML($name); 
    
    /**
    * Получить массив параметров шаблона
    * 
    * @param str $name 
    * @return array() | false
    */
    abstract protected function getSearchVar($name);
    
    /**
    * Получить html из шаблона
    * @param str $name - имя шаблона 
    * @param array $arrValues массив значений для замены array('{param1}' => val, '{param2}' =>val)
    * 
    * @return html
    */
    abstract protected function makeHTML($name, $arrValues);
   
}  
