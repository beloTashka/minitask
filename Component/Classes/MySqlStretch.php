<?php
namespace Component; 

/**
 * Класс подключается, для работы со второй базой данных MySQL
 * Одиночка, вызывается 
 * Component\MySqlStretch::getInstance()
 *
 * @name Component\MySqlStretch
 * 
 * @example
 * для соединения с сервером нужно в конфигурационном файле
 * задефайнить global attributes
 * define ("DBHOST", "*");
 * define ("STRETCH_DBNAME", "*");
 * define ("STRETCH_DBUSER", "*");
 * define ("STRETCH_DBPASS", "*");
 * define ("MYSERV", "*");
 *
 * для дальнейшей работы нужно наследовать этот класс
 */

 class MySqlStretch implements MemoryMethods
{
    private static $_instance = null;
    //---------------------------- global attributes-------------------------------
    private $_server = DBHOST;
    private $_user = STRETCH_DBUSER;
    private $_password = STRETCH_DBPASS;
    private $_db = STRETCH_DBNAME;
    private $__table = 'mod_mstream';
    private $_module;
    private $_myserv = MYSERV;
    //-----------------------------------------------------------------------------
    //----------------------------- local attributes-------------------------------
    var $_link;
    var $_rez;
    var $_row;
    var $_query = array('data' => '*', 'where' => '', 'group_by' => '', 'having' => '', 'order_by' => '', 'limit' => '');
    var $fields;
    var $sql;
    var $error;
    var $encoding = 'utf8';// utf8
    //-----------------------------------------------------------------------------

    /**
     * @return MySqlSketch
     */
    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }

    private function __construct()
    { 
        if ($this->connect() === false) {
            echo 'false connect db';
        } 

        return $this;
    }


    private function connect()
    {
        $this->_link = mysqli_connect($this->_server, $this->_user, $this->_password) 
            or $this->castError(mysqli_errno($this->_link), mysqli_error($this->_link));
        if (!$this->_link) {
            return false;
        }

        mysqli_set_charset($this->_link, $this->encoding);

        if (!mysqli_select_db($this->_link, $this->_db)) {
            return false;
        }
        return true;
    }


    public function disconnect()
    {
        mysqli_close($this->link);
    }


    private function freeresult()
    {

        $this->_rez = ""; # подавляем
    }

    public function set($key, $data)
    {
        $type = gettype($data);
        $data = serialize($data);
        $this->sql = "
            REPLACE INTO `{$this->__table}` 
            SET `key` = '{$key}'
                , `type` = '{$type}'
                , `value` = '{$data}' 
            ";
        return $this->query($this->sql);
    }

    public function get($key)
    {
        $this->sql = "SELECT `value` FROM `{$this->__table}` WHERE `key` = '{$key}' LIMIT 1";
        // echo $this->sql."<br><br>";
        $this->query($this->sql);
        $data = $this->get_row();
        return unserialize($data['value']);
    }

    public function delete($key)
    {
        $this->sql = "DELETE FROM `{$this->__table}`
        WHERE `key` = '{$key}'
        LIMIT 1";
        return $this->query($this->sql);
    }

    public function exists($key)
    {
        $data = $this->get($key);
        if ($data) {
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }


    public function query($string)
    {
        $result = false;

        $this->freeresult();
        $this->sql = $string;
        
        if ($this->_link) {
            $this->_rez = mysqli_query($this->_link, $string) or $this->castError(mysqli_error($this->_link)
                , $string);
        } 

        if ($this->_rez) {
            $result = true;
        }

        return $result;
    }

    /**
     * Получить результат запроса в виде массива
     *
     * @param str $query
     * @return array
     */
    public function query_fetch($query)
    {
        $result = array();
        $this->freeresult();
        $this->sql = $query;

        if ($this->_link) {
            $_rez = mysqli_query($this->_link, $query) or $this->castError(mysqli_error($this->_link), $query);
            $this->_rez = $_rez;
        }
        if($_rez) {
            while ($_row = mysqli_fetch_assoc($_rez)) {
                $result[] = $_row;
            }
        }
        return $result;
    }

    public function get_row($query_id = '')
    {
        if ($query_id == '') {
            $query_id = $this->_rez;
        }
        if($query_id != '') {
            $result = mysqli_fetch_assoc($query_id);  
        } else {
            $result = false;
        }
        return $result;
    }

    public function super_query($string)
    {
        $result = false;
        if ($this->query($string)) {
            if ($this->_rez !== false) {
                $result = mysqli_fetch_array($this->_rez, MYSQLI_ASSOC); // только ассоциотивный массив
                $this->_row = $result;
            }
        }
        return $result;
    }

    public function get_array()
    {
        $this->_row = mysqli_fetch_array($this->_rez);
        $result = $this->_row;
        return $result;
    }

    public function fetch($onlyFetchNoFillStructure = true)
    {
        $result = false;
        if ($this->_rez !== false) {
            $this->_row = mysqli_fetch_array($this->_rez);
            $result = $this->_row;
        }
        return $result;
    }

    public function count()
    {
        return ($this->_rez !== false) ? mysqli_num_rows($this->_rez) : 0;
    }

    public function _setLastError($error = "")
    {
        if ($error != "") {
            $this->error .= "\n" . $error;
        } else {
            $this->error .= mysqli_error($this->_link);
        }
    }

    public function getLastError()
    {
        return "<pre>" . $this->sql . "\n" . $this->error . "</pre>";
    }

    public function insert_id()
    {
        return mysqli_insert_id($this->_link);
    }

    public function safesql($source)
    {
        if ($this->_link) {
            return mysqli_real_escape_string($this->_link, $source);
        } else {
            return addslashes($source);
        }
    }

    public function castError($errorString, $query)
    {
        echo '<div style="border: 1px solid #ff0000; padding: 10px; background-color: #ffe7d4">';
        echo '<b>' . $errorString . '</b>';
        echo '<hr />' . $query;
        echo '</div>';
    }

}
