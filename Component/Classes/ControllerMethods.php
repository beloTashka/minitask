<?php
namespace Component;  

/**
 * Интерфейс контроллера отображения страницы сайта
 * 
 * Устанавливает необходимые универсальные методы 
 *
 * @name Component\ControllerMethods 
 */
interface ControllerMethods
{  
    /**
    * Инициализация с данными запроса пользователя
    * Валидация и краткий ответ о существовании страницы
    * 
    * @param int $lvl - уровень вложенности
    * @param array $requestUrl - url в виде массива
    * @param array $requestData - данные в массиве
    * @return bool
    */
    public function init($lvl, $requestUrl, $requestData);
    
    /**
    * Контент в виде html
    * @return html
    */
    public function getContent();
    
   /**
   * Действия в ответ на запрос пользователя
   *  
   */
    public function handler();
  
}  