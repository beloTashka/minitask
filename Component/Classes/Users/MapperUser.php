<?php
namespace Component\Users; 
use Component; 

/**
* Преобразователь данных из формата Сущности Пользователь 
* в формат реляционной db
* Обязательные поля для сохранения нового Пользователя
* login pass 
* @name \Component\Users\MapperUser 
*/

class MapperUser extends \Component\EntityMapper
{
    private $_user; 
    private $param; // Обязательные Параметры  
    
    private $limit_page = 20;
    
    private $tUser = 'users';
    private $tGroup = 'users_group';
    
    public function __construct($_user = null) 
    {
        $this->getStream();
        $this->param = array('login', 'pass');
        
        $this->pushUser($_user);
        
        return $this;
    }
    
    /**
    * Загрузить следующий объект Пользователь
    * в класс для последующей работы
    * @param  EntityUser | array $_user
    * @return MapperUser
    */
    public function pushUser($_user) 
    {
        if (isset($_user) 
            && !is_null($_user) 
            && gettype($_user) == 'object' 
            && get_class($_user) == 'EntityUser') {
            $this->_user = $_user;
        } else {
            $this->clear(); 
            if(is_array($_user) && count($user)) {
                foreach ($_user as $field => $value) {
                    $this->_user->__set($field, $value);
                } 
            }
        }
        
        return $this;
    }
    
    public function getGroup()
    {
        $result = false;
        $group = $this->_user->__get('group');
        if ($group && !is_null($group)) {
            $result = $group;
        }  
        return $result;
    }
    
    /**
    * Проверка загруженной в класс сущности Пользователь
    * на наличие обязательных параметров
    * $this->param
    * @return bool
    */
    public function checkUser()
    {
        $result = true;
        foreach($this->param as $field) {
            $value = $this->_user->__get($field);
            if(!$value || is_null($value) || $value == '') {
                $result = false;
            }
        }
        
        return $result;
    }
    
    public function checkPass($pass)
    {
        $result = false;
        $pass = md5($pass);
        if ($this->_user->__get('pass') == $pass) {
            $result = true;
        }
        return $result;
    }
    
    public function clear()
    {
        $this->_user = new EntityUser(); 
        return $this;
    }
    
    public function getUserByLogin($login)
    {
        $where = array();
        $where['login'] = $login;
        return $this->getUser($where);
    }
    
    private function getUser($where)
    {
        $result = false;
        $this->clear();
        $strWhere = $this->buildWhere($where); 
        $this->_query = "SELECT * FROM `{$this->tUser}` {$strWhere} LIMIT 1";
        $_user = $this->_stream->super_query($this->_query); 
        $this->pushUser($_user);
        if ($this->checkUser()) {
            $result = $this->_user;
        }
        return $result;
        
    }
    
    
    
    /**
    * Удалить данные сущности Пользователь из db
    */
    public function del()
    {}
    
    /**
    * Сохранить данные сущности Пользователь в db
    */
    public function save()
    {
        $result = false;
        return $result;
    }
    
}
