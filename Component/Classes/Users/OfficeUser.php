<?php
namespace Component\Users;

/**
*  
* @name \Component\Users\OfficeUser
*/

class OfficeUser extends \Component\Office 
{
    private $_mapperUser;
    
    public function __construct($requestData) 
    {
        $this->echo = true;
        
        $this->init(false, false, $requestData); 

        $this->_mapperUser = new MapperUser();
        $this->fillUpUser();
        
        return $this;
    }
    
    private function fillUpUser()
    {
        $result = false;
        if (isset($this->requestData['login']) && $this->requestData['login'] != '') {
            $this->_mapperUser->getUserByLogin($this->requestData['login']);
            if (isset($this->requestData['pass']) 
                && $this->requestData['pass'] != '') {
                $result = $this->_mapperUser->checkPass($this->requestData['pass']);    
            }   
        }        
        return $result;
    }
    
    public function groupAccess()
    {
        $result = false;
        if (isset($this->requestData['pass']) 
            && $this->requestData['pass'] != ''
            && $this->_mapperUser->checkPass($this->requestData['pass'])) {
            $result = $this->_mapperUser->getGroup();    
        } 
        return $result;       
    }
    
    public function checkPass($pass)
    {
        return $this->_mapperUser->checkPass($pass);
    }
    
    public function checkPassByLogin($login, $pass)
    {
        return $this->_mapperUser
                    ->getUserByLogin($login)
                    ->checkPass($pass);
    }
    
    public function getTplName()
    {}
    
    public function getDataView()
    {}
}
