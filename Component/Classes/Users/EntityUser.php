<?php
namespace Component\Users;  


/**
*  Класс описания сущности Дата
* @name \Component\Users\EntityUser
*/

class EntityUser extends \Component\EntityItem
{
    protected $id;
    protected $name; 
    protected $group;
    protected $login;
    protected $pass;
    protected $email;
    protected $cdate;
    protected $ban;
    protected $note;
    
    public function __construct() 
    {
        $fields = $this->fieldsAsArray();
        foreach($fields as $field) {
            $this->$field = null;
        }
        return $this;
    }
    
    protected function set_pass($pass)
    {
        $pass = md5($pass);
        if ($pass && $pass != '') {
            $this->pass = $pass;    
        }
        return $this;
    }
    
    protected function get_pass()
    {
        return false;   
    } 
}
