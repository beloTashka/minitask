<?php
namespace Component;  

/**
 * Абстрактный класс реализации сущности 
 * 
 * Устанавливает базовые методы 
 *
 * @name Component\EntityItem
 */
abstract class EntityItem implements EntityMethods
{
    public function __set($name, $value)
    {
        $result = NULL;
        $setter = 'set_' . $name;
        if (method_exists($this, $setter)) {
            $result = $this->$setter($value);
        } else if(property_exists($this, $name)) {
            $this->$name = $value;
        } 
        return $result;
    } 
    
    public function __get($name)
    {
        $result = NULL;
        $getter = 'get_' . $name;
        if (method_exists($this, $getter)) {
            $result = $this->$getter();
        } else if($this->__exists($name)) {
            $result = $this->$name;
        } 
        return $result;
    }
    
    public function __exists($name)
    {
        $result = false; 
        if(property_exists($this, $name) && !is_null($this->$name)) {
            $result = true;   
        }
        return $result;
    }
    
    public function toArray()
    {
        $result = array();
        $fields = get_object_vars($this);
        foreach ($fields as $field => $obj) {
            if(isset($obj) 
            && !is_null($obj) 
            && gettype($obj) == 'object') {
                $result[$field] = $obj->toArray(); 
            } else {
                $result[$field] = $obj;
            }      
        }
        return $result;
    }
    
    public function fieldsAsArray()
    {
        $result = array();
        $fields = get_object_vars($this);
        foreach ($fields as $field => $obj) {
            $result[] = $field;        
        }
        return $result;
    }

    
}  
