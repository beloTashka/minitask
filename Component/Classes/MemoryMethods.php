<?php
namespace Component;  

/**
 * Интерфейс потока оперативных данных
 * 
 * Устанавливает необходимые методы для работы с текущими данными
 * Во время сеанса работы пользователя
 *
 * @category Memory
 * @version 1.1
 * @name Component\MemoryMethods 
 * @author Наталия Белоцерковская <work.natawhite@gmail.com>  
 * @copyright Copyright (c) 2019 
 */
interface MemoryMethods
{
    /**
     * Добавление новых данных
     *
     * @param string $key Ключ / имя данных
     * @param string $type Тип данных
     * @param string $data набор данных
     * @return int
     */
     public function set($key, $data);
     
     /**
     * Получение данных
     *
     * @param string $key Ключ 
     * @param string $id 
     * @return array('type','data') 
     */
     public function get($key);
     
     /**
     * Проверка существования данных с ключом $key
     *
     * @param string $key Ключ 
     * @return int
     */
     public function exists($key);
     
     /**
     * Удаление записи с заданным ключом или/и id
     *
     * @param string $key Ключ 
     * @param string $id 
     * @return bool 
     */
     public function delete($key);
  
}  