<?php
namespace Component; 
 
/**
 * Ядро собственного http framework
 * Организация базового ЧПУ
 * Отображение информации по запросу пользователю,
 * основанная на настройках $routers config.router.php
 * реализуемая соответствующими контроллерами
 * 
 * @name Component\Kernel 
 */
class Kernel {
    private $router;

    use TraitUserQuery;
    use TraitDisplay; 
    
    
    public function __construct($router) 
    {
        $this->echo = false;
        $this->display('Kernel construct', 1);
        $this->defaultProperty();
        
        if ($router && is_array($router)) {
            $this->router = $router;
            
            if (isset($_SERVER['REDIRECT_URL']) && $_SERVER['REDIRECT_URL'] != '') {
                $this->requestUrl = explode('/', $_SERVER['REDIRECT_URL']); 
                $this->lvl = count($this->requestUrl) - 1;    
            }
            if (isset($_REQUEST) && is_array($_REQUEST) && count($_REQUEST) > 0) {
                $this->requestData = $_REQUEST;
            }
        }

        $this->requestUrl[0] = MYDOMEN; 
        
        return $this;
    } 
    
    private function defaultProperty()
    {
        $this->router = array();
        $this->requestUrl = array();
        $this->requestData = array();
        $this->lvl = 0;
        return $this;    
    }
    
    private function getControllerName()
    {
        $result = false;
        $strict = true; 
        $lvl = false;
        $index = 0; 
        do {
            if (isset($this->requestUrl[$index]) 
                    && isset($this->router[$index][$this->requestUrl[$index]]) ) {
                    $result = $this->router[$index][$this->requestUrl[$index]]['controller'];
                    $strict = $this->router[$index][$this->requestUrl[$index]]['next_strict'];
                    $lvl = $index;
            } else {
                $strict = false;
                $result = false; 
                $lvl = false;   
            }   
        } while ($strict && ++$index <= $this->lvl);
         
        if ($result && $lvl !== false && $lvl < $this->lvl) {
            $this->lvl = $lvl;
        }
        
        return $result;
    }
    
    
    public function getContent()
    {
        $result = '';
        $controllerName = $this->getControllerName();
        if ($controllerName) {
            $this->display($controllerName);

        } else {
            $controllerName = '\Component\Dayafterday\ControllerStart';
            http_response_code(404);
            $this->requestData['error'] = '404';
        }
        $controller = new $controllerName($this->lvl, $this->requestUrl, $this->requestData);
        $controller->handler(); 
        $result = $controller->getContent();
        return $result;
    }
}  

