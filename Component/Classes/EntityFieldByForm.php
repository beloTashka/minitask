<?php
namespace Component; 


/**
*  Класс описания сущности Дата
* @name \Component\EntityFieldByForm
*/

class EntityFieldByForm extends EntityItem
{
    private $name;
    private $title;
    private $value = false;
    private $type;
    private $inForm;
    private $limitFrom = false;
    private $limitTo = false;
    private $error = 0;
    private $mustBe;
     
    public function __construct($name, $title, $type, $limitFrom = false, $limitTo = false, $inForm = false, $mustBe = false) 
    {
        $this->name = $name;
        $this->title = $title;
        $this->type = $type;
        
        if ($limitFrom !== false) {
            $this->limitFrom = $limitFrom;
        }
        if ($limitTo !== false) {
            $this->limitTo = $limitTo;
        }  
        if ($inForm !== false) {
            $this->inForm = $inForm;
        } else {
            $this->inForm = 'input'; 
        } 
        if ($mustBe) {
            $this->mustBe = true;
        } else {
            $this->mustBe = false; 
        }    
        
        return $this;   
    }
    
    public function set_value($value)
    {
        $this->put_value($value)->put_error();
        return  $this;
    }
    
    public function get_value()
    {
        return $this->value;
    }
    
    private function put_value($value, $type = false) 
    {
        if(!$type) {
            $type = $this->type;
            $this->error = 0; 
        }
        
        switch ($type) {
            case 'tel':
                $value = preg_replace('/[^(\+\(\))\d]/ui', '', strval($value));
                $this->put_value($value, 'text'); 
            break;            
            case 'email':
                $this->put_value($value, 'text');
            break;
            case 'text':
                $value = strval($value);
                $len = mb_strlen($value);
                if($len > 0) {
                    if ($this->limitTo > 0 && $this->limitTo < $len) {
                        $this->value = mb_strcut($value, 0, $this->limitTo);
                    } else {
                        $this->value = $value;    
                    }
                } else {
                    $this->value = '';
                }
            break;
            default:  // float
                $this->value = (float)$value;
        }
        return $this; 
        
    }
    
    
    private function put_error()
    {
        if ($this->limitFrom !== false || $this->limitTo !== false) {
            switch ($this->type) {
                case 'tel':
                    $len = mb_strlen($this->value);
                    if($len > 0) {
                        if ($this->limitFrom > 0 && $this->limitFrom > $len) {
                            $this->error = 2;
                        }  
                    } else if ($this->mustBe) {
                        $this->error = 1; 
                    }
                break;                
                case 'email':
                    if ($this->value != '') {
                        if (!mb_strpos($this->value, '@')) {
                            $this->error = 2; 
                        } else if (!$this->checkEmail())  {
                            $this->error = 3;     
                        }
                    } else if ($this->mustBe) {
                        $this->error = 1; 
                    }
                break;
                case 'text':
                    $len = mb_strlen($this->value);
                    if($len > 0) {
                        if ($this->limitFrom > 0 && $this->limitFrom > $len) {
                            $this->error = 2;
                        }  
                    } else if ($this->mustBe && (!$this->value || $this->value == '')) {
                        $this->error = 1; 
                    }
                break;
                default:  // float
                    if ($this->limitFrom !== false && $this->value < $this->limitFrom) {
                        $this->error = 1;   
                    } 
                    if ($this->limitTo !== false && $this->value > $this->limitTo) {
                        if (!$this->error) { 
                            $this->error = 2;
                        } else {
                            $this->error = 3;    
                        }
                    }
            }
        }
        return $this;
    }
    
    private function checkEmail()
    {
        $result = false;
        $value = preg_replace('/[^(\_.\-\@)a-z\d]/ui', '',$this->value);
        if ($this->type == 'email' && $value == $this->value) {
            $result = true;    
        }  
        return $result;
    }
    
    public function toArray()
    {
        $result = get_object_vars($this); 
        return $result;
    }
}
