<?php
namespace Component; 
  
trait TraitPars {
    
    protected $startString;
    protected $endString;
    protected $allText;
    protected $slip;
    
    /**
    * Для начала цикла получения строк
    * 1. инициализация initSlipText
    * 2. получить следующий getNextCut (
    *   если предыдущий отрывок !== false && $this->getSlip() !== false
    * )
    */
    protected function initSlipText($startString = '', $endString = '', $allText = '', $slip = 0)
    {
        $this->startString = $startString;
        $this->endString = $endString;
        $this->allText = $allText;
        $this->slip = (int)$slip;
        return $this;
    }
    
    protected function getSlip()
    {
        return $this->slip;
    }
    
    /**
    * просмотр всего текста для нахождения требуемого отрывка
    * вспомогательная функция для работы с текстом
    * 
    */
    protected function getNextCut()
    {
        $result = false;
        $finishPos = false;
        
        $string = false;
        
        if ($this->slip !== false) {
            if ($this->startString == '') { 
                $startPos = $this->slip;
            } else {
                $startPos = strpos($this->allText, $this->startString, $this->slip);
            }

            if ($startPos !== false 
                && strpos($this->allText, $this->endString, $startPos) !== false) {
                $startPos = $startPos + strlen($this->startString);
                
                if ($this->endString == '') {
                    $string = substr($this->allText, $startPos);
                }else{
                    $finishPos = strpos($this->allText, $this->endString, $startPos); 
                    if ($finishPos !== false) {
                        $len = $finishPos - $startPos;
                        $string = substr($this->allText, $startPos, $len);     
                    } else {
                        $string = substr($this->allText, $startPos); 
                    }
                }  
                
                $result = $string;
            }
        }
        $this->slip = $finishPos; 
        return $result;   
    } 
    
    /**
    * Подготовка строки к записи в db
    * 
    * @param str $text
    * @param bool $html true - сохранить html
    * @return str
    */
    protected function sweepStr($text, $html = false, $sc = true)
    {

        
        $text = trim($text);
        
        if(!$html) {   
            $text = strip_tags($text);
        }
        if($html) {
            $text = preg_replace('/[^(<>=\/\\\?\_\s.,:;\-\*\[\])a-zа-яё\d]/ui', ' ',$text);
        } else {
            $text = preg_replace('/[^(\s._,:;\-\*\[\])a-zа-яё\d]/ui', ' ',$text);    
        }
        $text = preg_replace('/^\s{2,}/', ' ', $text);
        
        if($sc) {
            $text = htmlspecialchars($text);
        }
        $text = trim($text); 
        
        return $text;
    }
    
    protected function makeUrencode($url)
    {
         $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
         $replacements = array( '!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
         $result =  str_replace($entities, $replacements, urlencode(iconv('UTF-8','windows-1251',$url)));
         return str_replace('&amp;', '&', $result);
    }
}
