<?php
namespace Component; 
  
trait TraitDisplay {
    
    public $console = 0; // вариант запуска 0 - браузер, 1- сонсоль, 2 - cron (c логированием в файл)
    public $echo = false; // вывод текущих значений
    public $urlLogging = MYSERV .'/Logging/'; // '/var/www/html/Logging/';
    
        /**
    * Вывод значения на экран
    * @param mixed $val значение
    * @param int $type  as 1 echo, 2 var_dump, 3 print_r
    *  
    */
    public function display($val, $type = 3)
    {
        if($this->echo) {
            $delim = ($this->console)? "\r\n":'<br>';
            $txt = '';
            switch($type) {
                case 1:
                    if($this->console == 2) {
                        $txt .= $val;    
                    } else {
                        echo $val;
                        echo $delim;
                    }
                    break;
                case 2:
                    if($this->console == 2) {
                        $txt .= var_export($val); 
                        $txt .= $delim;   
                    } else {
                        var_dump($val);
                        echo $delim;
                    }
                    break;
                default:
                    if($this->console == 2) {
                        $txt .= var_export($val);    
                        $txt .= $delim;    
                    } else {
                        print_r($val);
                        echo $delim; 
                    }
            }
            if($this->console == 2) {  // запуск по крону, логирование в файл
                if($txt != '') {
                    $txt .= $delim; 
                } else {
                    $txt = 'Доступной информации о процессе нет.';
                }
                file_put_contents($this->urlLogging, $txt, FILE_APPEND); 
            } 
        }
        return $this;
    }
    
}
