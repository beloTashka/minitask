<?php
namespace Component; 
  
trait TraitUserQuery {
    
    protected $requestUrl;
    protected $requestData;
    protected $lvl;
    
    /**
    * Инициализация с данными запроса пользователя
    * Валидация и краткий ответ о существовании страницы
    * 
    * @param int $lvl - уровень вложенности
    * @param array $requestUrl - url в виде массива
    * @param array $requestData - данные в массиве
    * @return bool
    */
    public function init($lvl, $requestUrl, $requestData)
    {
        $lvl = (int)$lvl;
        $this->lvl = ($lvl > 0)? $lvl:0;
        $this->requestUrl = (is_array($requestUrl))? $requestUrl : array();
        $this->requestData = (is_array($requestData))? $requestData : array();
        
        return $this;
    }
    
}
