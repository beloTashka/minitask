<?php
namespace Component; 
  
trait Trait404 {
    
    protected $tpl404;
    
    /**
    * Инициализация с данными запроса пользователя
    * Валидация и краткий ответ о существовании страницы
    * 
    * @param int $lvl - уровень вложенности
    * @param array $requestUrl - url в виде массива
    * @param array $requestData - данные в массиве
    * @return bool
    */
    public function setTpl404($requestData)
    {
        if (isset($requestData['error']) 
            && mb_strpos($requestData['error'], '404') !== false) {
            $this->tpl404 = $requestData['error'];
        } else {
            $this->tpl404 = false;
        }
        
        return $this;
    }
    
    public function getTpl404()
    {
        $result = $this->tpl404;
        return $result;
    }
    
}
