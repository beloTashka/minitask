<?php

/**
* возможные обращения к сайту
* и их связь с контроллерами
* 
* @var $routers
* @since 2020.04.17
*/

$routers = array();

$routers[0][MYDOMEN] = array('controller' => '\Component\Dayafterday\ControllerStart', 'next_strict' => true);
$routers[1][''] = array('controller' => '\Component\Dayafterday\ControllerStart', 'next_strict' => true); 
$routers[1]['offer'] = array('controller' => '\Component\Dayafterday\ControllerStart', 'next_strict' => true); 
$routers[1]['test'] = array('controller' => '\Component\Dayafterday\ControllerStart', 'next_strict' => true);
$routers[1]['message'] = array('controller' => '\Component\Dayafterday\ControllerFormEmail', 'next_strict' => true);
// если следующие по иерархии заданы не строго и вычисляемы, управление передается контроллеру
$routers[1]['events'] = array('controller' => '\Component\Dayafterday\ControllerDays', 'next_strict' => false);



