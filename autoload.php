<?php
     
spl_autoload_register(function ($class) {
    // project-specific namespace prefix
    $prefix = PREFIX_TASKS;

    // base directory for the namespace prefix
    if (strpos($class, 'Trait') !== false) {
        $base_dir = COMTRAIT_DIR;     
    } else {
        $base_dir = COMCLASS_DIR;
    }
    
    
    // does the class use the namespace prefix?
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) === 0) {
        // get the relative class name
        $relative_class = substr($class, $len);
    
        $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
            
        if (file_exists($file)) { 
            require $file; 
        }
    }
}); 
